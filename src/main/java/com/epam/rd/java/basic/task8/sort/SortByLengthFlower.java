package com.epam.rd.java.basic.task8.sort;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;

public class SortByLengthFlower implements Comparator<Flower> {

    @Override
    public int compare (Flower f1, Flower f2) {
        return f1.getVisualParametersList ().get (0).getAveLenFlower () - f2.getVisualParametersList ().get (0).getAveLenFlower ();
    }
}
