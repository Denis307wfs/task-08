package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
    private final StringBuilder currentValue = new StringBuilder ();
    private Flowers flowers;
    private Flower flower;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;

    public SAXController (String xmlFileName) {}

    public Flowers getFlowers () {
        return flowers;
    }

    @Override
    public void startElement (String uri, String localName, String qName, Attributes attributes) {
        currentValue.setLength (0);
        if (qName.equalsIgnoreCase ("flowers")) {
            flowers = new Flowers ();
        }
        if (qName.equalsIgnoreCase ("flower")) {
            flower = new Flower ();
            flowers.getFlowerList ().add (flower);
        }
        if (qName.equalsIgnoreCase ("visualParameters")) {
            visualParameters = new VisualParameters ();
            flower.getVisualParametersList ().add (visualParameters);
        }
        if (qName.equalsIgnoreCase ("growingTips")) {
            growingTips = new GrowingTips ();
            flower.getGrowingTipsList ().add (growingTips);
        }
        if (qName.equalsIgnoreCase ("lighting")) {
            growingTips.setLighting (attributes.getValue (0));
        }
    }

    @Override
    public void endElement (String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase ("name")) {
            flower.setName (currentValue.toString ());
        }
        if (qName.equalsIgnoreCase ("soil")) {
            flower.setSoil (currentValue.toString ());
        }
        if (qName.equalsIgnoreCase ("origin")) {
            flower.setOrigin (currentValue.toString ());
        }
        if (qName.equalsIgnoreCase ("stemColour")) {
            visualParameters.setStemColour (currentValue.toString ());
        }
        if (qName.equalsIgnoreCase ("leafColour")) {
            visualParameters.setLeafColour (currentValue.toString ());
        }
        if (qName.equalsIgnoreCase ("aveLenFlower")) {
            visualParameters.setAveLenFlower (Integer.parseInt (currentValue.toString ()));
        }
        if (qName.equalsIgnoreCase ("temperature")) {
            growingTips.setTemperature (Integer.parseInt (currentValue.toString ()));
        }
        if (qName.equalsIgnoreCase ("watering")) {
            growingTips.setWatering (Integer.parseInt (currentValue.toString ()));
        }
        if (qName.equalsIgnoreCase ("multiplying")) {
            flower.setMultiplying (currentValue.toString ());
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {
        currentValue.append(ch, start, length);
    }
}