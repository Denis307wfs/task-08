package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    private int temperature;
    private String lighting;
    private int watering;

    public GrowingTips () {}

    public GrowingTips (int temperature, String lighting, int watering) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
    }

    public int getTemperature () {
        return temperature;
    }

    public void setTemperature (int temperature) {
        this.temperature = temperature;
    }

    public void setLighting (String lighting) {
        this.lighting = lighting;
    }

    public void setWatering (int watering) {
        this.watering = watering;
    }

    @Override
    public String toString () {
        final StringBuilder sb = new StringBuilder ();
        sb.append ("temperature=").append (temperature).append ("celcius");
        sb.append (", lighting='").append (lighting).append ('\'');
        sb.append (", watering=").append (watering).append ("mlPerWeek");
        return sb.toString ();
    }
}
