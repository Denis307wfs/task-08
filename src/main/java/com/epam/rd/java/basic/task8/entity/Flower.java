package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String multiplying;
    private final List<VisualParameters> visualParametersList = new ArrayList<> ();
    private final List<GrowingTips> growingTipsList = new ArrayList<> ();

    public List<VisualParameters> getVisualParametersList () {
        return visualParametersList;
    }

    public List<GrowingTips> getGrowingTipsList () {
        return growingTipsList;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getSoil () {
        return soil;
    }

    public void setSoil (String soil) {
        this.soil = soil;
    }

    public String getOrigin () {
        return origin;
    }

    public void setOrigin (String origin) {
        this.origin = origin;
    }

    public String getMultiplying () {
        return multiplying;
    }

    public void setMultiplying (String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString () {
        final StringBuilder sb = new StringBuilder ();
        sb.append ("\n");
        sb.append ("Flower{");
        sb.append ("name='").append (name).append ('\'');
        sb.append (", soil='").append (soil).append ('\'');
        sb.append (", origin='").append (origin).append ('\'');
        sb.append (", visualParameters=").append (visualParametersList);
        sb.append (", growingTips=").append (growingTipsList);
        sb.append (", multiplying='").append (multiplying).append ('\'');
        sb.append ('}');
        return sb.toString ();
    }
}
