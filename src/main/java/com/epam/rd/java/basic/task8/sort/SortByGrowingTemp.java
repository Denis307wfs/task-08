package com.epam.rd.java.basic.task8.sort;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;

public class SortByGrowingTemp implements Comparator<Flower> {

    @Override
    public int compare (Flower f1, Flower f2) {
        return f1.getGrowingTipsList ().get (0).getTemperature () - f2.getGrowingTipsList ().get (0).getTemperature ();
    }
}
