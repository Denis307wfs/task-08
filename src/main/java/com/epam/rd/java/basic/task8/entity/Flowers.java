package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    private List<Flower> flowerList = new ArrayList<> ();

    public List<Flower> getFlowerList () {
        return flowerList;
    }

    @Override
    public String toString () {
        final StringBuilder sb = new StringBuilder ("Flowers: ");
        sb.append (flowerList);
        return sb.toString ();
    }
}
