package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;

    public VisualParameters () {
    }

    public VisualParameters (String stemColour, String leafColour, int aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public void setStemColour (String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour (String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower () {
        return aveLenFlower;
    }

    public void setAveLenFlower (int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public String toString () {
        final StringBuilder sb = new StringBuilder ();
        sb.append ("stemColour='").append (stemColour).append ('\'');
        sb.append (", leafColour='").append (leafColour).append ('\'');
        sb.append (", aveLenFlower=").append (aveLenFlower).append ("cm");
        return sb.toString ();
    }
}
