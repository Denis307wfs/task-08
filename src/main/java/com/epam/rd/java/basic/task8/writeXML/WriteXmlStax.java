package com.epam.rd.java.basic.task8.writeXML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class WriteXmlStax {
    private String outputXmlFile;

    public WriteXmlStax (String outputXmlFile) {
        this.outputXmlFile = outputXmlFile;
        createDocument ();
    }

    public void createDocument () {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance ();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = docFactory.newDocumentBuilder ();
        } catch (ParserConfigurationException e) {
            e.printStackTrace ();
        }

        Document doc = docBuilder.newDocument ();
        Element rootElement = doc.createElement ("flowers");
        rootElement.setAttribute ("xmlns", "http://www.nure.ua");
        rootElement.setAttribute ("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute ("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        doc.appendChild (rootElement);

        createFirstFlower (doc, rootElement);
        createSecondFlower (doc, rootElement);

        // write dom document to a file
        try (FileOutputStream output = new FileOutputStream (outputXmlFile)) {
            writeXml (doc, output);
        } catch (IOException e) {
            e.printStackTrace ();
        }
    }

    private void writeXml (Document doc, OutputStream output) {

        TransformerFactory transformerFactory = TransformerFactory.newInstance ();
        Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer ();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace ();
        }
        transformer.setOutputProperty (OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource (doc);
        StreamResult result = new StreamResult (output);

        try {
            transformer.transform (source, result);
        } catch (TransformerException e) {
            e.printStackTrace ();
        }
    }

    public void createFirstFlower (Document doc, Element rootElement) {
        Element flower2 = doc.createElement ("flower");
        rootElement.appendChild (flower2);

        Element name2 = doc.createElement ("name");
        flower2.appendChild (name2);
        name2.setTextContent ("Rose");

        Element soil2 = doc.createElement ("soil");
        flower2.appendChild (soil2);
        soil2.setTextContent ("дерново-подзолистая");

        Element origin2 = doc.createElement ("origin");
        flower2.appendChild (origin2);
        origin2.setTextContent ("China");

        Element visualParameters2 = doc.createElement ("visualParameters");
        flower2.appendChild (visualParameters2);

        Element stemColour2 = doc.createElement ("stemColour");
        visualParameters2.appendChild (stemColour2);
        stemColour2.setTextContent ("green");

        Element leafColour2 = doc.createElement ("leafColour");
        visualParameters2.appendChild (leafColour2);
        leafColour2.setTextContent ("green");

        Element aveLenFlower2 = doc.createElement ("aveLenFlower");
        aveLenFlower2.setAttribute ("measure", "cm");
        aveLenFlower2.setTextContent ("10");
        visualParameters2.appendChild (aveLenFlower2);

        Element growingTips2 = doc.createElement ("growingTips");
        flower2.appendChild (growingTips2);

        Element temperature2 = doc.createElement ("temperature");
        temperature2.setAttribute ("measure", "celcius");
        temperature2.setTextContent ("25");
        growingTips2.appendChild (temperature2);

        Element lighting2 = doc.createElement ("lighting");
        lighting2.setAttribute ("lightRequiring", "yes");
        growingTips2.appendChild (lighting2);

        Element watering2 = doc.createElement ("watering");
        watering2.setAttribute ("measure", "mlPerWeek");
        watering2.setTextContent ("110");
        growingTips2.appendChild (watering2);

        Element multiplying2 = doc.createElement ("multiplying");
        flower2.appendChild (multiplying2);
        multiplying2.setTextContent ("черенки");
    }

    public void createSecondFlower (Document doc, Element rootElement) {
        Element flower = doc.createElement ("flower");
        rootElement.appendChild (flower);

        Element name = doc.createElement ("name");
        flower.appendChild (name);
        name.setTextContent ("Bambusa");

        Element soil = doc.createElement ("soil");
        flower.appendChild (soil);
        soil.setTextContent ("дерново-подзолистая");

        Element origin = doc.createElement ("origin");
        flower.appendChild (origin);
        origin.setTextContent ("China");

        Element visualParameters = doc.createElement ("visualParameters");
        flower.appendChild (visualParameters);

        Element stemColour = doc.createElement ("stemColour");
        visualParameters.appendChild (stemColour);
        stemColour.setTextContent ("green");

        Element leafColour = doc.createElement ("leafColour");
        visualParameters.appendChild (leafColour);
        leafColour.setTextContent ("green");

        Element aveLenFlower = doc.createElement ("aveLenFlower");
        aveLenFlower.setAttribute ("measure", "cm");
        aveLenFlower.setTextContent ("1100");
        visualParameters.appendChild (aveLenFlower);

        Element growingTips = doc.createElement ("growingTips");
        flower.appendChild (growingTips);

        Element temperature = doc.createElement ("temperature");
        temperature.setAttribute ("measure", "celcius");
        temperature.setTextContent ("30");
        growingTips.appendChild (temperature);

        Element lighting = doc.createElement ("lighting");
        lighting.setAttribute ("lightRequiring", "yes");
        growingTips.appendChild (lighting);

        Element watering = doc.createElement ("watering");
        watering.setAttribute ("measure", "mlPerWeek");
        watering.setTextContent ("250");
        growingTips.appendChild (watering);

        Element multiplying = doc.createElement ("multiplying");
        flower.appendChild (multiplying);
        multiplying.setTextContent ("черенки");
    }
}