package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {
	private Flowers flowers;
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;

	public STAXController (String xmlFileName) {
		try {
			printXmlByXmlCursorReader (Paths.get (xmlFileName));
		} catch (XMLStreamException e) {
			e.printStackTrace ();
		}
	}

	public Flowers getFlowers () {
		return flowers;
	}

	private void printXmlByXmlCursorReader(Path path) throws XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader = null;
		try {
			reader = xmlInputFactory.createXMLStreamReader(new FileInputStream (path.toFile()));
		} catch (XMLStreamException | FileNotFoundException ex) {
			ex.printStackTrace ();
		}

		int eventType;
		while (reader.hasNext()) {
			eventType = reader.next();
			if (eventType == XMLEvent.START_ELEMENT) {
				switch (reader.getName().getLocalPart()) {
					case "flowers":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							flowers = new Flowers ();
						}
						break;

					case "flower":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							flower = new Flower ();
							flowers.getFlowerList ().add (flower);
						}
						break;

					case "name":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							flower.setName (reader.getText());
						}
						break;

					case "soil":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							flower.setSoil (reader.getText());
						}
						break;

					case "origin":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							flower.setOrigin (reader.getText());
						}
						break;

					case "visualParameters":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							visualParameters = new VisualParameters ();
							flower.getVisualParametersList ().add (visualParameters);
						}
						break;

					case "stemColour":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							visualParameters.setStemColour (reader.getText());
						}
						break;

					case "leafColour":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							visualParameters.setLeafColour (reader.getText());
						}
						break;

					case "aveLenFlower":
						String measure = reader.getAttributeValue(null, "measure");
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							visualParameters.setAveLenFlower (Integer.parseInt (reader.getText()));
						}
						break;

					case "growingTips":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							growingTips = new GrowingTips ();
							flower.getGrowingTipsList ().add (growingTips);
						}
						break;

					case "temperature":
						String measureTemp = reader.getAttributeValue(null, "measure");
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							growingTips.setTemperature (Integer.parseInt (reader.getText()));
						}
						break;

					case "lighting":
						String requiring = reader.getAttributeValue(null, "lightRequiring");
						growingTips.setLighting (requiring);
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							growingTips.setLighting (reader.getText());
						}
						break;

					case "watering":
						String measureWat = reader.getAttributeValue(null, "measure");
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							growingTips.setWatering (Integer.parseInt (reader.getText()));
						}
						break;

					case "multiplying":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							flower.setMultiplying (reader.getText());
						}
						break;
				}
			}
		}
	}
}