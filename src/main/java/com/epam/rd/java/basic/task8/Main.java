package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.sort.SortByGrowingTemp;
import com.epam.rd.java.basic.task8.sort.SortByLengthFlower;
import com.epam.rd.java.basic.task8.sort.SortByName;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.writeXML.WriteXmlDom;
import com.epam.rd.java.basic.task8.writeXML.WriteXmlSax;
import com.epam.rd.java.basic.task8.writeXML.WriteXmlStax;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers flowersDOM = domController.parseFlowers ();

		// sort (case 1)
		Collections.sort (flowersDOM.getFlowerList (), new SortByName ());

		// save
		String outputXmlFile = "output.dom.xml";
		WriteXmlDom wrileXmlDom = new WriteXmlDom (outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(xmlFileName, saxController);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		// sort  (case 2)
		Collections.sort (saxController.getFlowers ().getFlowerList (), new SortByGrowingTemp ());

		// save
		outputXmlFile = "output.sax.xml";
		WriteXmlSax wrileXmlSax = new WriteXmlSax (outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> flowers = staxController.getFlowers ().getFlowerList ();

		// sort  (case 3)
		Collections.sort (staxController.getFlowers ().getFlowerList (), new SortByLengthFlower ());

		// save
		outputXmlFile = "output.stax.xml";
		WriteXmlStax writeXmlStax = new WriteXmlStax (outputXmlFile);
	}

}
