package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {
    private final String xmlFileName;
    private Document doc;

    public DOMController (String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Document buildDocument () {
        File file = new File (xmlFileName);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance ();
        try {
            doc = dbf.newDocumentBuilder ().parse (file);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace ();
        }

        return doc;
    }

    public Flowers parseFlowers () {
        Node rootNode = buildDocument ().getFirstChild ();
        Flowers flowers = new Flowers ();
        NodeList rootChilds = rootNode.getChildNodes ();

        for (int i = 0; i < rootChilds.getLength (); i++) {
            if (rootChilds.item (i).getNodeType () != Node.ELEMENT_NODE) {
                continue;
            }
            Flower flower = new Flower ();
            NodeList flowerChilds = rootChilds.item (i).getChildNodes ();
            parseFlowerChilds (flowerChilds, flower);

            flowers.getFlowerList ().add (flower);
        }

        return flowers;
    }

    public void parseFlowerChilds (NodeList flowerChilds, Flower flower) {
        for (int j = 0; j < flowerChilds.getLength (); j++) {
            if (flowerChilds.item (j).getNodeType () != Node.ELEMENT_NODE) {
                continue;
            }
            switch (flowerChilds.item (j).getNodeName ()) {
                case "name": {
                    String name = flowerChilds.item (j).getTextContent ();
                    flower.setName (name);
                    break;
                }
                case "soil": {
                    String soil = flowerChilds.item (j).getTextContent ();
                    flower.setSoil (soil);
                    break;
                }
                case "origin": {
                    String origin = flowerChilds.item (j).getTextContent ();
                    flower.setOrigin (origin);
                    break;
                }
                case "visualParameters": {
                    VisualParameters visualParameters = getVisualParameters (flowerChilds, j);
                    flower.getVisualParametersList ().add (visualParameters);
                    break;
                }
                case "growingTips": {
                    GrowingTips growingTips = getGrowingTips (flowerChilds, j);
                    flower.getGrowingTipsList ().add (growingTips);
                    break;
                }
                case "multiplying": {
                    String multiplying = flowerChilds.item (j).getTextContent ();
                    flower.setMultiplying (multiplying);
                    break;
                }
            }
        }
    }

    public VisualParameters getVisualParameters (NodeList flowerChilds, int j) {
        String stemColour = null;
        String leafColour = null;
        int aveLenFlower = 0;
        NodeList visualParametersChilds = flowerChilds.item (j).getChildNodes ();
        for (int k = 0; k < visualParametersChilds.getLength (); k++) {
            if (visualParametersChilds.item (k).getNodeType () != Node.ELEMENT_NODE) {
                continue;
            }
            switch (visualParametersChilds.item (k).getNodeName ()) {
                case "stemColour": {
                    stemColour = visualParametersChilds.item (k).getTextContent ();
                    break;
                }
                case "leafColour": {
                    leafColour = visualParametersChilds.item (k).getTextContent ();
                    break;
                }
                case "aveLenFlower": {
                    aveLenFlower = Integer.parseInt (visualParametersChilds.item (k).getTextContent ());
                    break;
                }
            }
        }

        return new VisualParameters (stemColour, leafColour, aveLenFlower);
    }

    public GrowingTips getGrowingTips (NodeList flowerChilds, int j) {
        int temperature = 0;
        String lighting = null;
        int watering = 0;
        NodeList growingTipsChilds = flowerChilds.item (j).getChildNodes ();
        for (int l = 0; l < growingTipsChilds.getLength (); l++) {
            if (growingTipsChilds.item (l).getNodeType () != Node.ELEMENT_NODE) {
                continue;
            }
            switch (growingTipsChilds.item (l).getNodeName ()) {
                case "temperature": {
                    temperature = Integer.parseInt (growingTipsChilds.item (l).getTextContent ());
                    break;
                }
                case "lighting": {
                    NamedNodeMap attributes = growingTipsChilds.item (l).getAttributes ();
                    Node namedItem = attributes.getNamedItem ("lightRequiring");
                    lighting = namedItem.getTextContent ();
                    break;
                }
                case "watering": {
                    watering = Integer.parseInt (growingTipsChilds.item (l).getTextContent ());
                    break;
                }
            }
        }

        return new GrowingTips (temperature, lighting, watering);
    }
}
